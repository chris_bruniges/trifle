
class CustardWrapper
  attr_reader :clazz
  def initialize(clazz:)
    @clazz = clazz
    
    clazz.methods.each do |method|
      send(:define_method, method) do |*args| 
        clazz.send(:foo, *args)
      end
    end
  end
end