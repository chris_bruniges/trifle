require_relative 'trifle'

  class Drawer
    def draw
      puts 'pretty thing'
    end
  end

  class DrawerMonitor
    def initialize(inner: Trifle.of(:draw))
      @inner = inner
    end

    def draw
      puts "Monitoring"
      @inner.draw
    end
  end

  def register_prod
    Trifle::Bowl.register Drawer.new
  end

  def register_debug
    Trifle::Bowl.register Drawer.new
    Trifle::Bowl.register DrawerMonitor.new
  end

  def run_prod
    register_prod
    Trifle.of(:draw).draw
    Trifle::Bowl.clear
  end

  def run_debug
    register_debug
    Trifle.of(:draw).draw
    Trifle::Bowl.clear
  end