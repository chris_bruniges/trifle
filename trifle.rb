module Trifle
  def self.of(*methods)
    Bowl.spoonful(*methods)
  end

  class Bowl
    @@things = Array.new
    @@method_implementers = {}

    def self.register(thing)
      @@things.push Registration.new thing
      thing.methods.each do |method|
        @@method_implementers[method] = [] unless @@method_implementers[method]
        @@method_implementers[method].push thing
      end
      true
    end

    def self.layer(clazz)
      result = nil
      @@things.each do |regi|
        result = regi.instance if regi.clazz <= clazz
      end
      result
    end

    def self.clear
      @@things = Array.new
      @@method_implementers = {}
    end

    def self.spoonful(*methods)
      sets_of_implementers = implementers_of_all *methods
      common_implementers = common_implementers sets_of_implementers
      common_implementers.any? ?  common_implementers.last : nil
    end

    private

    def self.common_implementers(sets_of_implementers)
      common_implementers = sets_of_implementers.first || []
      sets_of_implementers.each { |set| common_implementers &= set }
      common_implementers
    end

    def self.implementers_of_all(*methods)
      sets_of_implementers = []
      methods.each do |method|
        implementers = implementers_of method
        sets_of_implementers.push implementers if implementers
      end
      sets_of_implementers
    end

    def self.implementers_of(method)
      @@method_implementers[method]
    end
  end

  class Registration
    attr_reader :clazz
    attr_reader :instance

    def initialize(instance)
      @clazz = instance.class
      @instance = instance
    end
  end
end


