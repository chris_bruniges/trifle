require 'spec_helper'
require_relative '../trifle'

class Thing
  def dance
    'I am dancing'
  end
end

class ClappingThing < Thing
  def clap
    'I can CLAP'
  end
end

class Injected
  def initialize(clapper: Trifle.of(:clap))
    @clapper = clapper
    @dancer = Trifle.of(:dance)
  end

  def execute
    @clapper.clap
  end

  def execute_dance
    @dancer.dance
  end
end

describe Trifle::Bowl do
  before do
    Trifle::Bowl.clear
  end
  describe '@register' do
    it 'accepts instances' do
      expect(Trifle::Bowl.register(Object.new)).to be true
    end
  end

  describe '@layer' do
    context 'have registered a single thing' do
      let(:thing) { Thing.new }
      before { Trifle::Bowl.register thing }

      it 'retrieves the object' do
        expect(Trifle::Bowl.layer(Thing)).to eq thing
      end

      it 'retrieves a descendent of type if no exact match' do
        expect(Trifle::Bowl.layer(Object)).to eq thing
      end
    end

    context 'have registered two things' do
      let(:object) { Object.new }
      let(:thing) { Thing.new }
      before do
        Trifle::Bowl.register object
        Trifle::Bowl.register thing
      end

      it 'retrieves the last registered object descending from type' do
        expect(Trifle::Bowl.layer(Object)).to eq thing
      end

      it 'retrieves the other object' do
        expect(Trifle::Bowl.layer(Thing)).to eq thing
      end
    end

    context 'have registered the class twice' do
      let(:thing_1) { Thing.new }
      let(:thing_2) { Thing.new }
      before do
        Trifle::Bowl.register thing_1
        Trifle::Bowl.register thing_2
      end

      it 'retrieves the last instance' do
        expect(Trifle::Bowl.layer(Thing)).to eq thing_2
      end
    end
  end

  describe '@spoonful' do
    context 'have registered some instances' do
      let(:thing) { Thing.new }
      let(:object) { Object.new }
      let(:second_thing) { Thing.new }
      let(:clapping_thing) { ClappingThing.new }
      before do
        Trifle::Bowl.register object
        Trifle::Bowl.register second_thing
        Trifle::Bowl.register clapping_thing
        Trifle::Bowl.register thing
      end

      it 'can retrieve object by unique capability' do
        expect(Trifle::Bowl.spoonful(:dance)).to eq thing
      end

      it 'retrieves the last object registered implementing a non-unique method' do
        expect(Trifle::Bowl.spoonful(:methods)).to eq thing
      end

      it 'retrieves an object with combination of methods' do
        expect(Trifle::Bowl.spoonful(:dance, :methods)).to eq thing
      end

      it 'retrieves an object with combination of methods' do
        expect(Trifle::Bowl.spoonful(:clap, :methods)).to eq clapping_thing
      end

      it 'returns nil if it cannot find a match' do
        expect(Trifle::Bowl.spoonful(:unimplemented_method)).to be nil
      end
    end
  end

  describe '@of' do
    context 'have some things registered' do
      let(:thing) { Thing.new }
      let(:clapping_thing) { ClappingThing.new }
      let(:injected) { Injected.new }

      before do
        Trifle::Bowl.register clapping_thing
        Trifle::Bowl.register thing
        Trifle::Bowl.register injected
      end

      it 'can constructor injected' do
        expect(injected.execute).to eq clapping_thing.clap
      end

      it 'can be explicitly injected' do
        expect(injected.execute_dance).to eq thing.dance
      end

      it 'can build a dependency tree' do
        expect(Trifle.of(:execute).execute).to eq clapping_thing.clap
      end
    end
  end
end
